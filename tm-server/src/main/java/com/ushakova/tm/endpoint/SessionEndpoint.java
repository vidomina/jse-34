package com.ushakova.tm.endpoint;

import com.ushakova.tm.api.endpoint.ISessionEndpoint;
import com.ushakova.tm.api.service.ISessionService;
import com.ushakova.tm.model.Session;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @NotNull
    private ISessionService sessionService;

    public SessionEndpoint(@NotNull final ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    @WebMethod
    public void closeSession(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.close(session);
    }

    @WebMethod
    public @NotNull Session openSession(
            @NotNull @WebParam(name = "login") final String login,
            @NotNull @WebParam(name = "password") final String password
    ) {
        return sessionService.open(login, password);
    }

}

