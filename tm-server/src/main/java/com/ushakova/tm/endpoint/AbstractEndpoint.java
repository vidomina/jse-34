package com.ushakova.tm.endpoint;

import lombok.AccessLevel;
import lombok.Getter;

@Getter(value = AccessLevel.PROTECTED)
public class AbstractEndpoint {
}
