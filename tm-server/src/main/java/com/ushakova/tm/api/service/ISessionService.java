package com.ushakova.tm.api.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ushakova.tm.api.IService;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.model.Session;
import org.jetbrains.annotations.NotNull;

public interface ISessionService extends IService<Session> {

    @NotNull
    boolean checkDataAccess(@NotNull String login, @NotNull String password);

    boolean close(@NotNull Session session);

    @NotNull
    Session open(@NotNull String login, @NotNull String password);

    @NotNull
    Session sign(@NotNull Session session) throws JsonProcessingException;

    void validate(@NotNull Session session);

    void validate(@NotNull Session session, @NotNull Role role);

}