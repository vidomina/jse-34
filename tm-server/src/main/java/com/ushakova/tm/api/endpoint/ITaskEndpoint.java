package com.ushakova.tm.api.endpoint;

import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.model.Session;
import com.ushakova.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void add(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") Task entity
    );

    @WebMethod
    void addAll(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entities") List<Task> entities
    );

    @WebMethod
    @Nullable Task bindTaskToProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "taskId") final String taskId,
            @Nullable @WebParam(name = "projectId") final String projectId
    );

    @WebMethod
    @NotNull Task changeStatusById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "status") Status status
    );

    @WebMethod
    @NotNull Task changeStatusByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "status") Status status
    );

    @WebMethod
    @NotNull Task changeStatusByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "status") Status status
    );

    @WebMethod
    void clear(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    @NotNull Task completeById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Task completeByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Task completeByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    void create(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    );

    @WebMethod
    @NotNull List<Task> findAll(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    @NotNull Task findById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    );

    @WebMethod
    @Nullable Task findByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Task findByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    @Nullable Task removeById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    );

    @WebMethod
    @Nullable Task removeByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Task removeByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    @NotNull Task startById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Task startByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Task startByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    @Nullable Task unbindTaskFromProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "taskId") final String taskId
    );

    @WebMethod
    void updateById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    );

    @WebMethod
    void updateByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    );

}
