package com.ushakova.tm.api.entity;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

public interface IHasCreated {

    @NotNull
    Date getDateCreate();

    void setDateCreate(@NotNull Date date);

}
