package com.ushakova.tm.api.repository;

import com.ushakova.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    @Nullable
    List<Task> findAllByProjectId(@NotNull String projectId, @NotNull String userId);

}