package com.ushakova.tm.api.repository;

import com.ushakova.tm.api.IRepository;
import com.ushakova.tm.model.AbstractBusinessEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    @NotNull
    E add(@NotNull E entity, @NotNull String userId);

    void clear(@NotNull String userId);

    @Nullable
    List<E> findAll(@NotNull String userId);

    @Nullable
    List<E> findAll(@NotNull Comparator<E> comparator, @NotNull String userId);

    @Nullable
    E findById(@NotNull String id, @NotNull String userId);

    @Nullable
    E findByIndex(@NotNull Integer index, @NotNull String userId);

    @Nullable
    E findByName(@NotNull String name, @NotNull String userId);

    void remove(@NotNull E entity, @NotNull String userId);

    @Nullable
    E removeById(@NotNull String id, @NotNull String userId);

    @Nullable
    E removeByIndex(@NotNull Integer index, @NotNull String userId);

    @Nullable
    E removeByName(@NotNull String name, @NotNull String userId);

}
