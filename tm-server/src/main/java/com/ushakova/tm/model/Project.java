package com.ushakova.tm.model;

import com.ushakova.tm.api.entity.IWBS;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public class Project extends AbstractBusinessEntity implements IWBS {

    public Project(@NotNull String name) {
        this.name = name;
    }

    @Override
    @NotNull
    public String toString() {
        return "Id: " + this.getId() + "\nTitle: " + name
                + "\nDescription: " + description
                + "\nStatus: " + status.getDisplayName()
                + "\nStart Date:" + dateStart
                + "\nCreated: " + dateCreate;
    }

}