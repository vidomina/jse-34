package com.ushakova.tm.component;

import com.ushakova.tm.api.service.IDataService;
import com.ushakova.tm.api.service.IPropertyService;
import com.ushakova.tm.bootstrap.Bootstrap;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup {

    public static final String FILE_BINARY = "./data.bin";

    public static final String FILE_BASE64 = "./data.base64";

    public static final String FILE_FASTERXML_JSON = "./data-fasterxml.json";

    public static final String FILE_FASTERXML_XML = "./data-fasterxml.xml";

    public static final String FILE_FASTERXML_YAML = "./data-fasterxml.yaml";

    public static final String FILE_JAXB_JSON = "./data-jaxb.json";

    public static final String FILE_JAXB_XML = "./data-jaxb.xml";

    public static final String JAXB_JSON_PROPERTY_NAME = "eclipselink.media-type";

    public static final String JAXB_JSON_PROPERTY_VALUE = "application/json";

    public static final String SYSTEM_JSON_PROPERTY_NAME = "javax.xml.bind.context.factory";

    public static final String SYSTEM_JSON_PROPERTY_VALUE = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    public static final String BACKUP_XML = "./backup-data.xml";

    private static final String BACKUP_SAVE = "bkp-save";

    private static final String BACKUP_LOAD = "bkp-load";

    @NotNull
    final Bootstrap bootstrap;
    private final int INTERVAL;

    private final IDataService dataService;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    private IPropertyService propertyService;

    public Backup(
            @NotNull final Bootstrap bootstrap,
            @NotNull final IPropertyService propertyService,
            @NotNull final IDataService dataService
    ) {
        this.bootstrap = bootstrap;
        this.INTERVAL = propertyService.getBackupInterval();
        this.dataService = dataService;
    }

    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void load() {
        dataService.loadBackup();
    }

    public void save() {
        dataService.saveBackup();
    }

    public void stop() {
        es.shutdown();
    }

}