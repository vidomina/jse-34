package com.ushakova.tm.exception;

import org.jetbrains.annotations.NotNull;

public class AbstractException extends RuntimeException {

    protected String message;

    protected AbstractException(@NotNull String message) {
        this.message = message;
    }

    @Override
    @NotNull
    public String getMessage() {
        return message;
    }

}
