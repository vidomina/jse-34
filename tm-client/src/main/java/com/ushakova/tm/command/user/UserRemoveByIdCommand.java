package com.ushakova.tm.command.user;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.endpoint.User;
import com.ushakova.tm.exception.entity.UserNotFoundException;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class UserRemoveByIdCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Remove user by id.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("Enter user id:");
        @NotNull final String userId = TerminalUtil.nextLine();
        @Nullable final User user = endpointLocator.getAdminUserEndpoint().removeUserById(session, userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
    }

    @Override
    @NotNull
    public String name() {
        return "user-remove-by-id";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
