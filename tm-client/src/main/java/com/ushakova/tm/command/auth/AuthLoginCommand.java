package com.ushakova.tm.command.auth;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class AuthLoginCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Login";
    }

    @Override
    public void execute() {
        System.out.println("Enter Login:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter Password:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final Session session = endpointLocator.getSessionEndpoint().openSession(login, password);
        endpointLocator.setSession(session);
    }

    @Override
    @NotNull
    public String name() {
        return "login";
    }

}
