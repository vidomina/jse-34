package com.ushakova.tm.command.task;

import com.ushakova.tm.command.AbstractTaskCommand;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskCompleteTaskByIdCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Set \"Complete\" status to task by id.";
    }

    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("***Set Status \"Complete\" to Task***\nEnter Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        endpointLocator.getTaskEndpoint().completeById(session, id);
    }

    @Override
    @NotNull
    public String name() {
        return "complete-task-by-id";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
