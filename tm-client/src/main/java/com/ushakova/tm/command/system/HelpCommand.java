package com.ushakova.tm.command.system;

import com.ushakova.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public class HelpCommand extends AbstractCommand {

    @Override
    @Nullable
    public String arg() {
        return "-h";
    }

    @Override
    @Nullable
    public String description() {
        return "Show terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("\n***Help***");
        @NotNull final Collection<String> commands = endpointLocator.getCommandService().getCommandNameList();
        for (@NotNull final String command : commands) {
            System.out.println(command);
        }
    }

    @Override
    @NotNull
    public String name() {
        return "help";
    }

}
