package com.ushakova.tm;

import com.ushakova.tm.bootstrap.Bootstrap;
import com.ushakova.tm.util.SystemUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Application {

    public static void main(@Nullable String[] args) {
        System.out.println("PID: " + SystemUtil.getPID());
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.initCommands();
        bootstrap.initPID();
        bootstrap.run(args);
    }

}